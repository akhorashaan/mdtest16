FROM node:16-alpine3.14

WORKDIR /usr/src

COPY package*.json ./

RUN npm install

CMD ["npm", "run", "dev"]
